from django.db import transaction
from import_export import resources
from import_export.tmp_storages import CacheStorage
from import_export.admin import ImportExportModelAdmin

from django.contrib import admin, messages

from market_data.models import Stock, OptionQuote, StockPriceHistory, DailyStockPriceHistory


class StockResource(resources.ModelResource):

    class Meta:
        model = Stock
        fields = ('symbol', 'name')
        import_id_fields = ('symbol', 'name')
        export_order = ('symbol', 'name')


@admin.register(Stock)
class StockAdmin(ImportExportModelAdmin):

    resource_class = StockResource
    tmp_storage_class = CacheStorage

    search_fields = ('symbol', 'name')
    list_filter = ('active', 'benchmark')

    actions = ['use_as_benchmark']

    @transaction.atomic()
    def use_as_benchmark(self, request, queryset):
        """Mark stock as benchmark."""
        count = queryset.count()
        if count == 0:
            self.message_user(
                request,
                f'You need to select stock',
                level=messages.ERROR
            )
        elif count > 1:
            self.message_user(
                request,
                f'Only one stock can be used as benchmark, you selected {count}',
                level=messages.ERROR
            )
        Stock.objects.update(benchmark=False)
        queryset.update(benchmark=True)

    use_as_benchmark.short_description = 'Use selected stock as benchmark'


class OptionQuoteResource(resources.ModelResource):

    class Meta:
        model = OptionQuote
        exclude = ('id',)


@admin.register(OptionQuote)
class OptionQuoteAdmin(ImportExportModelAdmin):

    resource_class = OptionQuoteResource
    tmp_storage_class = CacheStorage
    list_filter = ('stock', 'type', 'expiration_date', 'quote_datetime')


class StockPriceHistoryResource(resources.ModelResource):

    class Meta:
        model = StockPriceHistory
        exclude = ('id',)


@admin.register(StockPriceHistory)
class StockPriceHistoryAdmin(ImportExportModelAdmin):

    resource_class = StockPriceHistoryResource
    tmp_storage_class = CacheStorage
    list_filter = ('stock', 'datetime')


class DailyStockPriceHistoryResource(resources.ModelResource):

    class Meta:
        model = DailyStockPriceHistory
        exclude = ('id',)


@admin.register(DailyStockPriceHistory)
class DailyStockPriceHistoryAdmin(ImportExportModelAdmin):

    resource_class = DailyStockPriceHistoryResource
    tmp_storage_class = CacheStorage
    list_filter = ('stock', 'date')
