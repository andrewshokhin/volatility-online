import pandas as pd
from bokeh.embed import components
from bokeh.layouts import row, gridplot
from bokeh.models import (
    ColumnDataSource, HoverTool, NumeralTickFormatter, FixedTicker, LinearAxis,
    DatetimeTickFormatter, Span
)
from bokeh.palettes import Category10
from bokeh.plotting import figure
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404, render_to_response
from django.views import View

from calculations.estimator import get_estimator
from market_data.models import Stock
from plots.forms import WindowForm
from stats.utils import get_daily_price_data


class VolatilityConesView(View):
    def get(self, request, stock_symbol):
        stock = get_object_or_404(Stock, symbol=stock_symbol)
        context = self.get_context_data(stock)
        return render_to_response('volatility_cones.html', context)

    def get_context_data(self, stock: Stock):
        plot = self.get_plot(stock)
        script, div = components(plot)
        return {'plot_script': script, 'plot_div': div,
                'selected_stock': stock}

    @staticmethod
    def get_plot(stock: Stock):
        price_data = get_daily_price_data(stock)
        windows = [30, 60, 90, 120]

        # get estimation
        estimators = pd.concat([get_estimator(window=window, price_data=price_data)
                                for window in windows],
                               axis=1, sort=False)

        estimators.columns = [str(w) for w in windows]

        # prepare aggregated data
        statistics = pd.DataFrame(columns=['q1', 'q2', 'q3', 'max', 'min', 'realized', 'iqr', 'upper', 'lower'])
        statistics['q1'] = estimators.quantile(0.25)
        statistics['q2'] = estimators.quantile(0.50)
        statistics['q3'] = estimators.quantile(0.75)
        statistics['max'] = estimators.max()
        statistics['min'] = estimators.min()
        statistics['realized'] = estimators.iloc[-1]
        statistics['iqr'] = statistics['q3'] - statistics['q1']
        statistics['upper'] = statistics['q3'] + 1.5 * statistics['iqr']
        statistics['lower'] = statistics['q3'] - 1.5 * statistics['iqr']
        statistics.index = statistics.index.astype(int)
        statistics.index.name = 'window'
        source = ColumnDataSource(statistics.sort_index())

        # Prepare left plot
        fig1 = figure(title=f'GarmanKlass estimator (daily from {price_data.index.min()} '
                            f'to {price_data.index.max()})',
                      x_axis_label='window',
                      plot_width=800, plot_height=500)

        tooltips = [('Window', '@window days')]
        formatters = {}
        for (column_name, description), color in zip([('max', 'Max'), ('q1', '25 Prctl'),
                                         ('q2', 'Median'), ('q3', '75 Prctl'),
                                         ('min', 'Min'), ('realized', 'Realized')], Category10[10]):
            fig1.circle(x='window', y=column_name, legend=description, color=color,
                        source=source)
            fig1.line(x='window', y=column_name, color=color, source=source)
            tooltips.append((description, f'@{column_name}{{0%}}'))
            formatters[column_name] = 'numeral'
        fig1.add_tools(HoverTool(tooltips=tooltips, formatters=formatters))
        fig1.legend.location = "bottom_left"
        fig1.yaxis.formatter = NumeralTickFormatter(format='0%')
        fig1.xaxis.ticker = FixedTicker(ticks=windows)
        # Prepare right plot
        # https://bokeh.pydata.org/en/latest/docs/gallery/boxplot.html
        labels = [str(w) for w in windows]
        fig2 = figure(title='Cones',
                      x_axis_label='window', x_range=labels,
                      plot_width=200, plot_height=500)

        fig2.segment(labels, statistics['upper'], labels, statistics['q3'], line_color="black")
        fig2.segment(labels, statistics['lower'], labels, statistics['q1'], line_color="black")

        fig2.vbar(labels, 0.2, statistics['q2'], statistics['q3'], fill_color="#E08E79", line_color="black")
        fig2.vbar(labels, 0.2, statistics['q1'], statistics['q2'], fill_color="#3B8686", line_color="black")

        fig2.rect(labels, statistics['lower'], 0.2, 0.001, line_color="black")
        fig2.rect(labels, statistics['upper'], 0.2, 0.001, line_color="black")
        # move y-axis to the right
        fig2.yaxis.visible = False
        fig2.add_layout(LinearAxis(formatter=NumeralTickFormatter(format='0 %')), 'right')

        # concatenate two plots
        return row(fig1, fig2)


class RollingDescriptivesView(View):

    @staticmethod
    def get_window_options():
        return [(30, '30 days'), (60, '60 days'), (90, '90 days'), (120, '120 days')]

    def get(self, request, stock_symbol):
        stock = get_object_or_404(Stock, symbol=stock_symbol)
        form = WindowForm(request.GET)
        if form.is_valid():
            window = form.cleaned_data['window']
            if not window:
                window = 30
            context = self.get_context_data(stock, window)
            return render_to_response('rolling_descriptives.html', context)
        else:
            return HttpResponseBadRequest(form.errors.as_json())

    def get_context_data(self, stock: Stock, window: int):
        plot = self.get_plot(stock, window)
        script, div = components(plot)
        return {'plot_script': script, 'plot_div': div,
                'selected_stock': stock, 'selected_window': window,
                'window_options': self.get_window_options()}

    @staticmethod
    def get_plot(stock: Stock, window: int):
        price_data = get_daily_price_data(stock)
        # get estimator
        estimator = get_estimator(
            window=window,
            price_data=price_data
        )
        descriptives = pd.DataFrame(estimator, columns=['realized'])

        descriptives['mean'] = estimator.rolling(window=window, center=False).mean()
        descriptives['std'] = estimator.rolling(window=window, center=False).std()
        descriptives['zscore'] = (estimator - descriptives['mean']) / descriptives['std']
        descriptives['top_q'] = estimator.rolling(window=window, center=False).quantile(0.25)
        descriptives['median'] = estimator.rolling(window=window, center=False).median()
        descriptives['bottom_q'] = estimator.rolling(window=window, center=False).quantile(0.75)
        last = estimator[-1]

        # prepare first plot
        fig1 = figure(title=f'GarmanKlass estimator (daily from {price_data.index.min()} '
                            f'to {price_data.index.max()})',
                      x_axis_label='Date',
                      plot_width=800, plot_height=500)

        source = ColumnDataSource(descriptives)
        tooltips = [('Date', '@date{%F}')]
        formatters = {'date': 'datetime'}
        for (column_name, description), color in zip([('top_q', '25 Prctl'), ('median', 'Median'),
                                                      ('bottom_q', '75 Prctl'), ('mean', 'Mean'),
                                                      ('realized', 'Realized')],
                                                     Category10[10]):
            fig1.line(x='date', y=column_name, color=color, legend=description, source=source)
            tooltips.append((description, f'@{column_name}{{0.00%}}'))
            formatters[column_name] = 'numeral'

        fig1.legend.location = "bottom_left"
        fig1.yaxis.formatter = NumeralTickFormatter(format='0 %')
        fig1.xaxis.formatter = DatetimeTickFormatter()
        fig1.legend.click_policy = "hide"
        fig1.add_tools(HoverTool(tooltips=tooltips, formatters=formatters))

        # prepare second plot
        # https://bokeh.pydata.org/en/latest/docs/gallery/boxplot.html
        label = ['realized']
        fig2 = figure(title='Realized (with last value)', x_range=label,
                      plot_width=200, plot_height=500)

        q1 = estimator.quantile(q=0.25)
        q2 = estimator.quantile(q=0.5)
        q3 = estimator.quantile(q=0.75)
        iqr = q3 - q1
        upper = q3 + 1.5 * iqr
        lower = q1 - 1.5 * iqr

        fig2.segment(label, upper, label, q3, line_color="black")
        fig2.segment(label, lower, label, q1, line_color="black")

        fig2.vbar(label, 0.2, q2, q3, fill_color="#E08E79", line_color="black")
        fig2.vbar(label, 0.2, q1, q2, fill_color="#3B8686", line_color="black")

        fig2.rect(label, lower, 0.2, 0.001, line_color="black")
        fig2.rect(label, upper, 0.2, 0.001, line_color="black")

        fig2.circle(label, last, size=10, color="red", fill_alpha=0.6)

        # move y-axis to the right
        fig2.yaxis.visible = False
        fig2.add_layout(LinearAxis(formatter=NumeralTickFormatter(format='0 %')), 'right')

        # prepare third plot
        fig3 = figure(title='Z-score',
                      plot_width=800, plot_height=200)
        fig3.line(x='date', y='zscore', color='magenta', source=source)
        # create a horizontal line at y=0
        fig3.add_layout(Span(location=0, dimension='width', line_color='black', line_width=1))

        fig3.xaxis.formatter = DatetimeTickFormatter()
        fig3.add_tools(HoverTool(
            tooltips=[('Date', '@date{%F}'), ('Z-score', '@zscore')],
            formatters={'date': 'datetime', 'zscore': 'numeral'}, mode='vline'
        ))

        # prepare forth plot
        fig4 = figure(title='Std. dev', plot_width=800, plot_height=200)
        fig4.line(x='date', y='std', color='green', source=source)
        fig4.xaxis.formatter = DatetimeTickFormatter()
        fig4.add_tools(HoverTool(
            tooltips=[('Date', '@date{%F}'), ('Std dev', '@std{0.00%}')],
            formatters={'date': 'datetime', 'std': 'numeral'}, mode='vline'
        ))

        # combine plot together
        return gridplot([[fig1, fig2], [fig4],  [fig3]])
