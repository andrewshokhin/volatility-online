import math
import logging
from time import sleep
from typing import List
from datetime import datetime, date, time, timedelta
from urllib.parse import urljoin

import requests

from market_data.models import OptionQuote, Stock, StockPriceHistory, DailyStockPriceHistory

logger = logging.getLogger(__name__)


class TradierClient:
    """Helper class for interaction with tradier.com API"""

    def __init__(self, base_api_url: str, access_token: str):
        self._base_api_url = base_api_url
        self._access_token = access_token
        self._session = None
        # when last time we started requesting API
        # necessary for rate limiting handling
        self._last_quota_start = datetime.now()

    @property
    def session(self) -> requests.Session:
        if self._session is None:
            self._session = requests.Session()
            self._session.headers.update(
                {'Authorization': f'Bearer {self._access_token}',
                 'Accept': 'application/json'}
            )
        return self._session

    def _send_request(self, path: str):
        full_path = urljoin(self._base_api_url, path)
        logger.info(f'Sending request {full_path}')
        response = self.session.get(full_path)
        if not response.ok:
            if self._last_quota_start is None:
                logger.info(f'Second try is unsuccessful, got {response.status_code} status code')
                # it's a second try
                response.raise_for_status()
            if response.status_code == 400:
                # first unsuccessful request, sleep till next minute start and try again
                since_last_quota_start = (datetime.now() - self._last_quota_start).total_seconds() % 60
                # five extra seconds, just in case
                sleep_time = math.ceil(60 - since_last_quota_start + 5)
                logger.info(f'Request throttled, sleeping for {sleep_time} seconds')
                sleep(sleep_time)
                self._last_quota_start = None
                return self._send_request(path)
            else:
                # unknown error
                logger.info(f'Got {response.status_code} status code')
                response.raise_for_status()
        else:
            if self._last_quota_start is None:
                # second try is successful, continue
                logger.info('Second try is successful')
                self._last_quota_start = datetime.now()
            else:
                logger.info('Request is successful')

        return response.json()

    def request_expiration_dates(self, stock) -> List[date]:
        """Return available expiration dates for specified stock."""
        response_data = self._send_request(f'markets/options/expirations?symbol={stock.symbol}')
        expirations = response_data.get('expirations')
        if not expirations:
            return []
        return [datetime.strptime(date_string, '%Y-%m-%d').date()
                for date_string in expirations.get('date', [])]

    def request_quotes(self, stock: Stock, expiration_date: date) -> List[OptionQuote]:
        """Return quotes for specified stock and expiration date."""
        response_data = self._send_request(f'markets/options/chains?symbol={stock.symbol}'
                                           f'&expiration={expiration_date.isoformat()}')
        quotes = []
        for quote in response_data['options']['option']:
            # bid_date is timestamp in microseconds
            quote_datetime = datetime.fromtimestamp(quote['bid_date'] // 1000)
            expiration_datetime = datetime.combine(expiration_date, time.max)
            time_until_expiration = expiration_datetime - quote_datetime
            if quote['bid'] is None or quote['ask'] is None:
                mid = 0
            elif quote['bid'] == 0 or quote['ask'] == 0:
                mid = quote['last']
            else:
                mid = (quote['bid'] + quote['ask']) / 2
            if mid is None:
                continue
            quotes.append(OptionQuote(
                # "IBM181214C00113000"
                identifier=quote['symbol'],
                stock_id=stock.id,
                # description is "IBM Dec 14 2018 $104.00 Put"
                # where $104.00 is a strike
                type=quote['description'].split(' ')[-1].lower(),
                strike=quote['strike'],
                last=quote['last'],
                bid=quote['bid'],
                ask=quote['ask'],
                mid=mid,
                volume=quote['volume'],
                open_interest=quote['open_interest'],
                interest_rate=0,
                quote_datetime=quote_datetime,
                expiration_date=expiration_datetime.date(),
                days_until_expiration=time_until_expiration.days,
                time_fraction_until_expiration=time_until_expiration / timedelta(days=365)
            ))
        return quotes

    def request_price_history(self, stock: Stock) -> List[StockPriceHistory]:
        """Return same day trading history for specified stock."""
        response_data = self._send_request(f'markets/timesales?symbol={stock.symbol}&interval=15min')
        stock_price_history = []
        for tick in response_data['series']['data']:
            stock_price_history.append(StockPriceHistory(
                stock_id=stock.id,
                datetime=datetime.strptime(tick['time'], '%Y-%m-%dT%H:%M:%S'),
                price=tick['price'],
                open=tick['open'],
                high=tick['high'],
                low=tick['low'],
                close=tick['close'],
                volume=tick['volume'],
            ))
        return stock_price_history

    def request_daily_price_data(self, stock: Stock) -> List[DailyStockPriceHistory]:
        """
        Tradier API also supports start_date argument in this method
        but it much slower with it.
        """
        url = f'markets/history?symbol={stock.symbol}'
        response_data = self._send_request(url)
        if not response_data.get('history'):
            # no new history records
            return []
        daily_prices = []
        for daily_data in response_data['history']['day']:
            daily_prices.append(DailyStockPriceHistory(
                stock=stock,
                date=datetime.strptime(daily_data['date'], '%Y-%m-%d').date(),
                open=daily_data['open'],
                high=daily_data['high'],
                low=daily_data['low'],
                close=daily_data['close'],
                volume=daily_data['volume'],
            ))
        return daily_prices

