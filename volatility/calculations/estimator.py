"""
Taken from https://github.com/jasonstrimpel/volatility-trading/blob/master/volatility/models/GarmanKlass.py
"""
import math

import numpy as np


def get_estimator(window, price_data, clean=True):
    """
    GarmanKlassEstimator

    Parameters
    ----------
    window : int
        Rolling window for which to calculate the estimator
    price_data: pd.DataFrame
        DataFrame with 'high', 'low' and 'close' columns
    clean : boolean
        Set to True to remove the NaNs at the beginning of the series

    Returns
    -------
    y : pandas.DataFrame
        Estimator series values
    """

    log_hl = (price_data['high'] / price_data['low']).apply(np.log)
    log_cc = (price_data['close'] / price_data['open']).apply(np.log)

    rs = 0.5 * log_hl ** 2 - (2 * math.log(2) - 1) * log_cc ** 2

    trading_periods = price_data.shape[0]

    def f(v):
        return (trading_periods * v.mean()) ** 0.5

    # from pandas doc:
    # By default, the result is set to the right edge of the window
    result = rs.rolling(window=window, center=False).apply(func=f, raw=True)

    if clean:
        return result.dropna()
    else:
        return result
