from django.db import models


class QuoteStats(models.Model):
    """
    Contains additional data describing quotes.

    It's separated from the original model because it can be changed over time
    unlike historical market data.
    """
    id = models.BigAutoField(primary_key=True)
    quote = models.OneToOneField('market_data.OptionQuote', on_delete=models.CASCADE,
                                 related_name='stats')
    implied_volatility = models.FloatField()
    delta_greek = models.FloatField()


class RealizedVolatility(models.Model):
    stock = models.ForeignKey('market_data.Stock', on_delete=models.CASCADE,
                              related_name='realized_volatility')
    date = models.DateField()
    value = models.FloatField()

    class Meta:
        unique_together = [('stock', 'date')]
