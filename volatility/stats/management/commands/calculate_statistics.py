import logging
from datetime import date
from typing import Optional

import pandas as pd
from django.core.management import BaseCommand
from django.db import transaction, DatabaseError
from django.db.models import Max

from calculations.estimator import get_estimator
from calculations.greeks import calculate_delta
from calculations.implied_volatility import calculate_implied_volatility
from market_data.models import Stock
from stats.utils import get_daily_price_data
from stats.models import QuoteStats, RealizedVolatility

logger = logging.getLogger(__name__)
# default value for realized volatility calculation
REALIZED_VOLATILITY_WINDOW = 30


class Command(BaseCommand):

    help = ('Calculates several statistics for freshly downloaded market data.\n'
            'Should be called after `load_market_data` command')

    def handle(self, *args, **options):
        all_stocks = Stock.objects.filter(active=True)
        stocks_count = all_stocks.count()
        logger.info(f'Processing {stocks_count} stocks')
        for i, stock in enumerate(all_stocks, 1):
            logger.info(f'Calculating statistics for "{stock.symbol}" stock ({i} of {stocks_count})')
            try:
                self.process_stock(stock)
            except (DatabaseError, ValueError, AttributeError, TypeError, KeyError):
                logger.exception(f'Got an error processing {stock.symbol}, skipping',
                                 exc_info=True)
        logger.info('All stocks have benn processed')

    @staticmethod
    def process_stock(stock: Stock):
        quotes = calculate_iv_and_delta(stock)
        if quotes is not None:
            save_quotes_stats(quotes)
        realized = calculate_realized_volatility(stock)
        if realized is not None:
            save_realized_volatility(stock, realized)


def calculate_iv_and_delta(stock: Stock) -> Optional[pd.DataFrame]:
    """
    Take quotes from the database and calculate implied_volatility and
    delta_greek statistics for them.
    """
    quotes = pd.DataFrame.from_records(
        stock.option_quotes.filter(expiration_date__gte=date.today())
        .values('id', 'type', 'strike', 'mid', 'quote_datetime', 'interest_rate',
                'time_fraction_until_expiration', 'expiration_date', 'type')
        .order_by('quote_datetime'),
    )
    if quotes.empty:
        logger.info(f'No actual quotes found for "{stock.symbol}" stock')
        return
    quotes = quotes.set_index('quote_datetime')
    prices = get_prices_for_quotes(stock, quotes)
    quotes = calculate_implied_volatility(quotes, prices)
    quotes = calculate_delta(quotes)
    return quotes


def get_prices_for_quotes(stock: Stock, quotes: pd.DataFrame):
    """Take prices of underlying stocks for specified quotes."""
    prices = pd.DataFrame.from_records(
        stock.price_history.filter(datetime__range=(quotes.index.min(),
                                                    quotes.index.max()))
        .values('datetime', 'close').order_by('datetime'),
    )
    prices = prices.rename(columns={'close': 'underlying_price'})
    if prices.empty:
        # no data for time range, look for data before
        prices = pd.DataFrame.from_records(
            stock.price_history.filter(datetime__lte=quotes.index.min())
            .values('datetime', 'close').order_by('datetime')[:1],
        )
        prices = prices.rename(columns={'close': 'underlying_price'})
    if prices.empty:
        # no data before, look for data after interval
        prices = pd.DataFrame.from_records(
            stock.price_history.filter(datetime__gte=quotes.index.max())
            .values('datetime', 'open').order_by('datetime')[:1],
        )
        prices = prices.rename(columns={'open': 'underlying_price'})
    if prices.empty:
        # no prices data at all
        raise RuntimeError(f'Stock {stock} has no pricing data')
    return prices.set_index('datetime')


def save_quotes_stats(quotes: pd.DataFrame):
    """Save calculated statistics to the database."""
    with transaction.atomic():
        QuoteStats.objects.filter(quote_id__in=quotes['id']).delete()
        QuoteStats.objects.bulk_create([
            QuoteStats(quote_id=quote['id'],
                       implied_volatility=quote['implied_volatility'],
                       delta_greek=quote['delta'])
            for _, quote in quotes.iterrows()
        ])


def calculate_realized_volatility(stock: Stock) -> Optional[pd.Series]:
    """Take prices history and calculate realized volatility."""
    price_data = get_daily_price_data(stock)
    if price_data.empty:
        return
    estimator = get_estimator(
        window=REALIZED_VOLATILITY_WINDOW,
        price_data=price_data
    )
    return pd.Series(estimator, name='realized')


def save_realized_volatility(stock: Stock, realized: pd.Series):
    """Save new records into the database."""
    last_realized = stock.realized_volatility.aggregate(last=Max('date')).get('last')
    if last_realized and last_realized in realized.index:
        realized = realized[last_realized:].iloc[1:]
    RealizedVolatility.objects.bulk_create([
        RealizedVolatility(stock=stock, date=day, value=value)
        for day, value in realized.iteritems()
    ])
