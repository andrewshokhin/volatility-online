from datetime import date

from functools import lru_cache
from typing import List

from market_data.models import Stock, OptionQuote


@lru_cache(maxsize=32)
def get_expiration_dates(stock: Stock = None, future_only=False) -> List[date]:
    """Return a list of available expiration dates in database."""
    qs = OptionQuote.objects.all()
    if stock is not None:
        qs = qs.filter(stock=stock)
    if future_only:
        qs = qs.filter(expiration_date__gt=date.today())
    return list(qs.distinct('expiration_date')
                .order_by('expiration_date')
                .values_list('expiration_date', flat=True))
