from typing import Tuple

import pandas as pd
from bokeh.embed import components
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, NumeralTickFormatter, DatetimeTickFormatter, HoverTool
from bokeh.plotting import figure
from django.shortcuts import get_object_or_404, render_to_response
from django.views import View

from calculations.estimator import get_estimator
from market_data.models import Stock
from stats.utils import get_daily_price_data


def get_daily_price_data_and_benchmark(stock: Stock, benchmark_stock: Stock
                                       ) -> Tuple[pd.DataFrame, pd.DataFrame]:
    price_data = get_daily_price_data(stock)
    benchmark = get_daily_price_data(benchmark_stock)
    # ensure stock and benchmark have the same index
    intersection = price_data.index.intersection(benchmark.index)
    price_data = price_data.loc[intersection]
    benchmark = benchmark.loc[intersection]
    return price_data, benchmark


class BenchmarkComparisonView(View):

    def get(self, request, stock_symbol):
        stock = get_object_or_404(Stock, symbol=stock_symbol)
        context = self.get_context_data(stock)
        return render_to_response('benchmark_comparison.html', context)

    def get_context_data(self, stock: Stock):
        plot = self.get_plot(stock)
        script, div = components(plot)
        return {'plot_script': script, 'plot_div': div,
                'selected_stock': stock}

    @staticmethod
    def get_plot(stock: Stock):
        benchmark_stock = Stock.get_benchmark()
        price_data, benchmark = get_daily_price_data_and_benchmark(
            stock, benchmark_stock)

        # get estimators
        window = 30
        estimator = get_estimator(
            window=window,
            price_data=price_data
        )
        benchmark_estimator = get_estimator(
            window=window,
            price_data=benchmark
        )
        dates = estimator.index

        # Create first plot
        fig1 = figure(
            title=f'GarmanKlass estimator {stock.symbol} VS {benchmark_stock.symbol} '
                  f'(daily from {dates.min()} to {dates.max()})',
            x_axis_label='Date',
            plot_width=600, plot_height=400)
        source = ColumnDataSource({'date': dates, 'current_stock': estimator,
                                   'benchmark_stock': benchmark_estimator,
                                   'ratio': estimator / benchmark_estimator})

        fig1.line(x='date', y='current_stock', legend=f'{stock}', color='red', source=source)
        fig1.line(x='date', y='benchmark_stock', legend=f'{benchmark_stock}', color='green',
                  source=source)
        fig1.legend.location = "bottom_left"
        fig1.yaxis.formatter = NumeralTickFormatter(format='0%')
        fig1.xaxis.formatter = DatetimeTickFormatter()
        fig1.add_tools(HoverTool(
            tooltips=[(stock.symbol, '@current_stock{0%}'),
                      (benchmark_stock.symbol, '@benchmark_stock{0%}'),
                      ('Date', '@date{%F}')],
            formatters={'current_stock': 'numeral', 'date': 'datetime',
                        'benchmark_stock': 'numeral'}, mode='vline'))

        # Create second plot
        fig2 = figure(title=f'Ratio', x_axis_label='Date',
                      plot_width=600, plot_height=200)
        fig2.line(x='date', y='ratio', color='blue', source=source)
        fig2.yaxis.formatter = NumeralTickFormatter(format='0 %')
        fig2.xaxis.formatter = DatetimeTickFormatter()
        fig2.add_tools(HoverTool(
            tooltips=[('Ratio', '@ratio{0%}'), ('Date', '@date{%F}')],
            formatters={'ratio': 'numeral', 'date': 'datetime'}, mode='vline'))
        # stack them
        return column(fig1, fig2)


class BenchmarkCorrelationView(View):

    def get(self, request, stock_symbol):
        stock = get_object_or_404(Stock, symbol=stock_symbol)
        context = self.get_context_data(stock)
        return render_to_response('benchmark_correlation.html', context)

    def get_context_data(self, stock: Stock):
        plot = self.get_plot(stock)
        script, div = components(plot)
        return {'plot_script': script, 'plot_div': div,
                'selected_stock': stock}

    @staticmethod
    def get_plot(stock: Stock):
        benchmark_stock = Stock.get_benchmark()
        price_data, benchmark = get_daily_price_data_and_benchmark(
            stock, benchmark_stock)

        # get estimators
        window = 30
        estimator = get_estimator(
            window=window,
            price_data=price_data
        )
        benchmark_estimator = get_estimator(
            window=window,
            price_data=benchmark
        )
        dates = estimator.index
        correlation = estimator.rolling(window=window).corr(other=benchmark_estimator)

        fig = figure(title=f'Correlation of {stock.symbol} and {benchmark_stock.symbol}'
                           f'(daily from {dates.min()} to {dates.max()})',
                     x_axis_label='Date',
                     plot_width=600, plot_height=400)
        source = ColumnDataSource({'date': dates, 'correlation': correlation})
        fig.line(x='date', y='correlation', color='blue', source=source)
        fig.yaxis.formatter = NumeralTickFormatter(format='0 %')
        fig.xaxis.formatter = DatetimeTickFormatter()
        fig.add_tools(HoverTool(
            tooltips=[('Corr.', '@correlation{0%}'), ('Date', '@date{%F}')],
            formatters={'correlation': 'numeral', 'date': 'datetime'}, mode='vline'))
        return fig