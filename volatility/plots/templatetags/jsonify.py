"""
https://djangosnippets.org/snippets/201/
"""
import json

from django.core.serializers.json import DjangoJSONEncoder
from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django import template

register = template.Library()


@register.filter()
def jsonify(obj):
    if isinstance(obj, QuerySet):
        return serialize('json', obj)
    return json.dumps(obj, cls=DjangoJSONEncoder)
