FROM gcr.io/google_appengine/python

ENV PYTHONUNBUFFERED 1

ENV PATH /env/bin:$PATH
RUN virtualenv -p python3 /env


# Copying & Installing python requirements
ADD Pipfile /app/Pipfile
ADD Pipfile.lock /app/Pipfile.lock
RUN pip install pipenv
RUN pipenv install --system --deploy

# Copy source files
ADD volatility /app/volatility
ADD manage.py /app

# Run the gunicorn web server
CMD gunicorn -b :$PORT volatility.wsgi --pythonpath /app/volatility/