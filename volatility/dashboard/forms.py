from django import forms


class StockAutoCompleteForm(forms.Form):
    search = forms.SlugField(required=False)
    page = forms.IntegerField(required=False)
