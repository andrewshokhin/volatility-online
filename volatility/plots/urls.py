from django.urls import path

from . import views

app_name = 'plots'
urlpatterns = [
    path('skew_chart/<slug:stock_symbol>', views.SkewChartView.as_view(),
         name='skew_chart'),
    path('term_structure/<slug:stock_symbol>', views.TermStructureView.as_view(),
         name='term_structure'),

    path('volatility_cones/<slug:stock_symbol>', views.VolatilityConesView.as_view(),
         name='volatility_cones'),
    path('rolling_descriptives/<slug:stock_symbol>', views.RollingDescriptivesView.as_view(),
         name='rolling_descriptives'),

    path('benchmark_comparison/<slug:stock_symbol>', views.BenchmarkComparisonView.as_view(),
         name='benchmark_comparison'),
    path('benchmark_correlation/<slug:stock_symbol>', views.BenchmarkCorrelationView.as_view(),
         name='benchmark_correlation'),

    path('universe_explorer/', views.UniverseExplorerView.as_view(),
         name='universe_explorer'),

]
