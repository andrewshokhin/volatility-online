var selected_section = 'skew_section';
var selected_stock;
$('.section-link').on(
    'click', function (e) {
        var new_selected_section = e.target.id;
        if (selected_section === new_selected_section){
            return
        }
        $('#title').text(e.target.text);
        $('#' + selected_section).removeClass('active');
        $(e.target).addClass('active');
        selected_section = new_selected_section;
        load_plots();
    }
);

function load_plots() {
    if(selected_section === 'skew_section'){
        if (selected_stock == null){
            $('#plots').text('Select stock');
            return
        }
        $('#plots').replaceWith(
            '<div id="plots">' +
            '<div id="skew_chart"></div>\n' +
            '<div id="term_structure"></div>' +
            '</div>'
        );
        $("#skew_chart").load('/plots/skew_chart/' + selected_stock);
        $("#term_structure").load('/plots/term_structure/' + selected_stock);
    }else if (selected_section === 'richness_section'){
        if (selected_stock == null){
            $('#plots').text('Select stock');
            return
        }
        $('#plots').replaceWith(
            '<div id="plots">' +
            '<div id="volatility_cones"></div>\n' +
            '<div id="rolling_descriptives"></div>' +
            '</div>'
        );
        $("#volatility_cones").load('/plots/volatility_cones/' + selected_stock);
        $("#rolling_descriptives").load('/plots/rolling_descriptives/' + selected_stock);
    }else if (selected_section === 'benchmark_section'){
        if (selected_stock == null){
            $('#plots').text('Select stock');
            return
        }
        $('#plots').replaceWith(
            '<div id="plots">' +
            '<div id="benchmark_comparison"></div>\n' +
            '<div id="benchmark_correlation"></div>' +
            '</div>'
        );
        $("#benchmark_comparison").load('/plots/benchmark_comparison/' + selected_stock);
        $("#benchmark_correlation").load('/plots/benchmark_correlation/' + selected_stock);
    }else if (selected_section === 'explorer_section'){
        $('#plots').replaceWith(
            '<div id="plots">' +
            '<div id="universe_explorer"></div>\n' +
            '</div>'
        );
        $("#universe_explorer").load('/plots/universe_explorer/');
    }

}
$('#stock_search').select2({
    ajax: {
        url: '/stock-autocomplete/',
        data: function (params) {
            var query = {
                search: params.term,
                page: params.page || 1
            };
            return query;
        }
    },
    width: '100%',
    theme: "dark-adminlte"
}).on('select2:select', function (e){
    selected_stock = e.params.data.id;
    load_plots();
});