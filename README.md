# volatility-online



## How to deploy
Application is hosted in Google Kubernetes Engine (gke) with 
Cloud SQL backend (postgresql) according to https://cloud.google.com/python/django/kubernetes-engine

To update application do the following:

* Set database settings 
```bash
export POSTGRES_DB_NAME=volatility_online_db
export POSTGRES_USER=cloud_sql_user
export POSTGRES_PASSWORD=[[PASSOWRD]]
export POSTGRES_DB_PORT=5433
```
* Run sql cloud proxy
```bash
./cloud_sql_proxy -instances=volatility-online:australia-southeast1:volatility-online-db-instance=tcp:5433   
```
* Run migrations
```bash
./manage.py migrate
```

* Update static files
```bash
./manage.py collectstatic --noinput
gsutil -m rsync -R static/  gs://volatility-online-bucket/static  
```

* Build and push docker image:
```bash
docker build -t gcr.io/volatility-online/volatility-online-app:[[TAG]]  .
docker push  gcr.io/volatility-online/volatility-online-app:[[TAG]]  
```

* Update `image` attributes in `gke-resources.yaml`

* Update web-server deployment and (or) cronjob in kubernetes
```bash
kubectl apply -f gke-resources.yaml 
```

## How to setup cluster from scratch

```bash
gcloud container clusters create volatility-online-shared-cluster \
    --scopes "https://www.googleapis.com/auth/userinfo.email","cloud-platform" \
    --num-nodes 2 --machine-type=g1-small --zone "australia-southeast1-c" \
    --enable-ip-alias
gcloud container clusters get-credentials volatility-online-shared-cluster  --zone "australia-southeast1-c"
docker build -t gcr.io/volatility-online/volatility-online-app .
docker push  gcr.io/volatility-online/volatility-online-app
kubectl create secret generic cloudsql --from-literal=username=[[DB_USER]] \
    --from-literal=password=[[DB_PASS]] --from-literal=host=[[DB_PRIVATE_IP]]
kubectl create secret generic tradier-api --from-literal=base_url=[[BASE_API]] \
    --from-literal=access_token=[[ACCESS_TOKEN]] 
kubectl create -f gke-resources.yaml
```
