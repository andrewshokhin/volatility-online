# Generated by Django 2.1.4 on 2018-12-13 00:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('market_data', '0002_dailystockpricehistory'),
    ]

    operations = [
        migrations.AddField(
            model_name='stock',
            name='benchmark',
            field=models.BooleanField(default=False),
        ),
    ]
