import time
from datetime import datetime

import pandas as pd
import numpy as np
from scipy.interpolate import interp1d


def _get_days_until_expiration(df: pd.DataFrame):
    """ Return the number of days until expiration

    :param df: row of the dataframe, accessible by label
    :return: days until expiration

    """

    expiration = df['Expiration']

    # add the hours to the expiration date so we get the math correct
    date_str = expiration.strftime('%Y-%m-%d') + ' 23:59:59'

    # convert date string into datetime object
    expiry = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")

    # get today
    today = datetime.today()

    # return the difference and add one to count for today
    return (expiry - today).days


def _get_time_fraction_until_expiration(series):
    """ Return the fraction of a year until expiration

    :param series: row of the dataframe, accessible by label
    :return: fraction of a year until expiration

    """

    expiration = series['Expiration']

    # add the hours to the expiration date so we get the math correct
    date_str = expiration.strftime('%Y-%m-%d') + ' 23:59:59'

    # convert date string into datetime object
    time_tuple = time.strptime(date_str, "%Y-%m-%d %H:%M:%S")

    # get the number of seconds from the epoch until expiration
    expiry_in_seconds_from_epoch = time.mktime(time_tuple)

    # get the number of seconds from the epoch to right now
    right_now_in_seconds_from_epoch = time.time()

    # get the total number of seconds to expiration
    seconds_until_expiration = expiry_in_seconds_from_epoch - right_now_in_seconds_from_epoch

    # seconds in year
    seconds_in_year = 31536000.0

    # fraction of seconds to expiration to total in year, rounded
    return max(seconds_until_expiration / seconds_in_year, 1e-10)


terms = [30, 3 * 30, 6 * 30, 12 * 30, 24 * 30, 36 * 30, 60 * 30]
rates = [0.0001, 0.0009, 0.0032, 0.0067, 0.0097, 0.0144, 0.0184]


def _get_rate(series):
    """ Interpolate rates out to 30 years
        Note computing rates like this is not strictly theoretically
        correct but works for illustrative purposes

    :param series: row of the dataframe, accessable by label
    :return interpolated interest rate based on term structure

    """
    days = series['DaysUntilExpiration']

    # generate terms for every thirty days up until our longest expiration
    new_terms = [i for i in range(30, (60 * 30) + 1)]

    # create linear interpolation model
    f = interp1d(terms, rates, kind='linear')

    # interpolate the values based on the new terms we created above
    ff = f(new_terms)

    # return the interpolated rate given the days to expiration
    return round(ff[max(days, 30) - 30], 8)


def _get_mid(series):
    """ Get the mid price between bid and ask

    :param series: row of the dataframe, accessable by label
    :return mid price

    """
    bid = series['Bid']
    ask = series['Ask']
    last = series['Last']

    # if the bid or ask doesn't exist, return 0.0
    if np.isnan(ask) or np.isnan(bid):
        return 0.0

    # if the bid or ask are 0.0, return the last traded price
    elif ask == 0.0 or bid == 0.0:
        return last
    else:
        return (ask + bid) / 2.0


def apply_helper_functions(options_frame):
    options_frame['DaysUntilExpiration'] = options_frame.apply(_get_days_until_expiration, axis=1)
    options_frame['TimeUntilExpiration'] = options_frame.apply(_get_time_fraction_until_expiration, axis=1)
    options_frame['InterestRate'] = options_frame.apply(_get_rate, axis=1)
    options_frame['Mid'] = options_frame.apply(_get_mid, axis=1)
    return options_frame


def interp_implied_volatility(options_frame):
    """ Interpolate missing (np.nan) values of implied volatility
    We first need to split the chains into expiration and type because we cannot
    interpolate across the entire chain, rather within these two groups

    :param options_frame: DataFrame containing options data
    :return original DataFrame with ImpliedVolatilityMid column containing interpolated values

    """
    # create a MultiIndex with Expiration, OptionType, the Strike as index, then sort
    frame = options_frame.set_index(['Expiration', 'OptionType', 'Strike']).sort_index()

    # pivot the vame with ImpliedVolatilityMid as the values within the table
    # this has Strikes along the rows and Expirations along the columns
    # the level=1 unstack pivots on Expiration and level=0 unstack pivots on OptionType
    unstacked = frame['ImpliedVolatilityMid'].unstack(level=1).unstack(level=0)

    # this line does three things:
    #    first interpolates across each Expiration date down the strikes for np.nan values
    #    second forward fills values which keeps the last interpolated value as the value to fill
    #    third back fills values which keeps the first interpolated value as the value to fill
    unstacked_interp = unstacked.interpolate().ffill().bfill()

    # restack into shape of original DataFrame
    unstacked_interp_indexed = unstacked_interp.stack(level=0).stack(level=0).reset_index()

    # replace old column with the new column with interpolated and filled values
    frame['ImpliedVolatilityMid'] = unstacked_interp_indexed.set_index(['Expiration', 'OptionType', 'Strike'])

    # give our index back
    frame.reset_index(inplace=True)

    # return
    return frame
