from django.http import JsonResponse, HttpResponseBadRequest
from django.views.generic import TemplateView
from django.views import View
from django.db.models import Q

from market_data.models import Stock
from dashboard.forms import StockAutoCompleteForm


class StockAutocomplete(View):
    """
    API method compatible with select2 interface
    https://select2.org/data-sources/ajax
    """
    page_size = 10

    def get(self, request):
        qs = Stock.objects.all()
        form = StockAutoCompleteForm(request.GET)
        if form.is_valid():
            search = form.cleaned_data['search']
            if search:
                qs = qs.filter(Q(name__icontains=search) | Q(symbol__icontains=search))

            page_number = form.cleaned_data['page']
            if not page_number:
                page_number = 1

            page = qs.order_by('name')[(page_number - 1) * self.page_size: page_number * self.page_size]
            result = [{'id': stock.symbol, 'text': f'{stock}'} for stock in page]
            more_results = qs[page_number * self.page_size:].exists()
            return JsonResponse({'results': result, 'pagination': {'more': more_results}})
        else:
            return HttpResponseBadRequest(form.errors.as_json())


class IndexView(TemplateView):

    template_name = 'index.html'
