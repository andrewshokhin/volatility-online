from bokeh.resources import CDN, INLINE
from django import template
from django.utils.safestring import SafeText

register = template.Library()


@register.simple_tag()
def load_bokeh():
    """Loads javascript and css for bokeh lib"""
    return SafeText(INLINE.render())
