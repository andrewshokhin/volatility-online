from datetime import date
from itertools import cycle
from typing import List

import pandas as pd
from bokeh.embed import components
from bokeh.models import ColumnDataSource, HoverTool, DatetimeTickFormatter
from bokeh.plotting import figure
from bokeh.palettes import Category10
from django.db.models import Count, Q
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404, render_to_response
from django.views import View

from market_data.models import Stock, OptionQuote
from plots.forms import ExpirationDatesListForm, StrikeForm
from plots.views.utils import get_expiration_dates


class SkewChartView(View):

    def get(self, request, stock_symbol):
        stock = get_object_or_404(Stock, symbol=stock_symbol)
        form = ExpirationDatesListForm(request.GET)
        if form.is_valid():
            expiration_dates = form.cleaned_data['expiration_dates']
            if not expiration_dates:
                # find the nearest expiration date
                expiration_dates = get_expiration_dates(stock, future_only=True)[:5]
            context = self.get_context_data(stock, expiration_dates)
            return render_to_response('skew_chart.html', context)
        else:
            return HttpResponseBadRequest(form.errors.as_json())

    def get_context_data(self, stock: Stock, expiration_dates: List[date]):
        plot = self.get_plot(stock, expiration_dates)
        script, div = components(plot)
        available_dates = ','.join(
            d.isoformat() for d in get_expiration_dates(stock)
        )
        selected_dates = ','.join(
            d.isoformat() for d in expiration_dates
        )

        return {'plot_script': script, 'plot_div': div,
                'selected_stock': stock, 'selected_dates': selected_dates,
                'available_dates': available_dates}

    @staticmethod
    def get_plot(stock: Stock, expiration_dates: List[date]):
        quotes = pd.DataFrame.from_records(
            stock.option_quotes
            .filter(expiration_date__in=expiration_dates)
            # prefetch stats calculated offline
            .filter(stats__isnull=False).values('strike', 'expiration_date',
                                                'stats__implied_volatility')
        )
        if quotes.empty:
            return
        quotes = quotes.rename(columns={'stats__implied_volatility': 'implied_volatility'})
        implied_volatility = quotes.pivot_table(
            index='strike',
            columns='expiration_date',
            values='implied_volatility'
        )
        # bokeh cuts lines when sees NaN, so interpolate date before plotting
        implied_volatility = implied_volatility.interpolate()
        # we also need to assign column names to valid identifiers
        dates = implied_volatility.columns
        columns = {d: f'column_{n}' for n, d in enumerate(dates)}
        implied_volatility = implied_volatility.rename(columns=columns)

        source = ColumnDataSource(implied_volatility)
        tooltips = [('Strike', '@strike')]
        plot = figure(title='Skew Chart for five nearest expiration dates',
                      x_axis_label='Strike', y_axis_label='Implied Volatility',
                      plot_width=1000, plot_height=500)
        for (expiration_date, column_name), color in zip(columns.items(), cycle(Category10[10])):
            date_str = expiration_date.isoformat()
            plot.line(x='strike', y=column_name, color=color, legend=date_str, source=source)
            tooltips.append((date_str, f'@{column_name}'))
        plot.add_tools(HoverTool(tooltips=tooltips))
        plot.legend.click_policy = "hide"
        return plot


class TermStructureView(View):

    def get(self, request, stock_symbol):
        stock = get_object_or_404(Stock, symbol=stock_symbol)
        form = StrikeForm(request.GET)
        if form.is_valid():
            strike = form.cleaned_data['strike']
            if strike is None:
                # find strike with the most number of quotes
                strike = (stock.option_quotes
                          .filter(expiration_date__gt=date.today(), type=OptionQuote.CALL)
                          .values('strike').annotate(cnt=Count('id'))
                          .order_by('-cnt')[0]
                          .get('strike'))
            context = self.get_context_data(stock, strike)
            return render_to_response('term_structure.html', context)
        else:
            return HttpResponseBadRequest(form.errors.as_json())

    def get_available_strike_values(self, stock: Stock):
        """
        Return a list of all values for "column_name" in database
        having at least several records in database.
        """
        minimal_records_count = 5
        return (
            stock.option_quotes
            .filter(expiration_date__gt=date.today(), type=OptionQuote.CALL)
            .values('strike').annotate(cnt=Count('id'))
            .filter(cnt__gte=minimal_records_count)
            .values_list('strike', flat=True)
        )

    def get_context_data(self, stock: Stock, strike: float):
        plot = self.get_plot(stock, strike)
        script, div = components(plot)
        available_strikes = ','.join(
            "{0:.2f}".format(s) for s in self.get_available_strike_values(stock)
        )

        return {'plot_script': script, 'plot_div': div,
                'selected_stock': stock, 'selected_strike': strike,
                'available_strikes': available_strikes}

    @staticmethod
    def get_plot(stock: Stock, strike: float):
        delta = 0.01
        term_structure = pd.DataFrame.from_records(
            stock.option_quotes.filter(
                # float can loose accuracy somewhere
                # so select strike from interval
                Q(strike__gt=strike - delta) & Q(strike__lt=strike + delta),
                type=OptionQuote.CALL
            ).filter(stats__isnull=False)
            # prefetch stats calculated offline
            .values('expiration_date', 'stats__implied_volatility')
            .order_by('expiration_date')
        )
        if term_structure.empty:
            return
        term_structure = term_structure.rename(
            columns={'stats__implied_volatility': 'implied_volatility'}
        )

        plot = figure(title='Term structure', x_axis_label='Expiration date',
                      y_axis_label='Implied Volatility',
                      plot_width=1000, plot_height=500)
        source = ColumnDataSource(term_structure)
        plot.line(x='expiration_date', y='implied_volatility', source=source)
        plot.circle(x='expiration_date', y='implied_volatility', source=source)
        plot.add_tools(HoverTool(
            tooltips=[('Exp. date', '@expiration_date{%F}'), ('IV', '@implied_volatility')],
            formatters={'expiration_date': 'datetime'}, mode='vline'
        ))
        plot.xaxis.formatter = DatetimeTickFormatter()
        return plot
