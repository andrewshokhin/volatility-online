from django.db import models


class Stock(models.Model):
    # Full name like "Intel Corporation"
    name = models.TextField()
    # short name like "INTC"
    symbol = models.TextField(unique=True)
    # flag showing if stock should be updated by `load_market_data` command
    active = models.BooleanField(default=True)
    # flag showing which stock should be used as a benchmark
    # there should be only one record with this flag
    benchmark = models.BooleanField(default=False)

    @classmethod
    def get_benchmark(cls):
        try:
            return cls.objects.get(benchmark=True)
        except cls.DoesNotExist:
            raise RuntimeError('No stock marked as benchmark. '
                               'Please select one in admin site.')
        except cls.MultipleObjectsReturned:
            raise RuntimeError('More than one stock is marked as benchmark. '
                               'Please select one in admin site.')

    def __str__(self):
        return f'{self.symbol} ({self.name})'


class OptionQuote(models.Model):
    PUT = 'put'
    CALL = 'call'
    TYPE_CHOICES = [(PUT, 'Put'), (CALL, 'Call')]

    # unique identifier of the quote like "IBM181214C00113000"
    # needed to detect new quotes and prevent duplicates
    identifier = models.TextField(unique=True)
    # market data
    stock = models.ForeignKey(Stock, on_delete=models.SET_NULL, null=True,
                              related_name='option_quotes')
    type = models.CharField(choices=TYPE_CHOICES, max_length=4)
    strike = models.FloatField()
    last = models.FloatField(null=True)
    bid = models.FloatField()
    ask = models.FloatField()
    mid = models.FloatField()
    volume = models.FloatField()
    interest_rate = models.FloatField()
    open_interest = models.FloatField()
    # underlying_price = models.FloatField()
    quote_datetime = models.DateTimeField()
    expiration_date = models.DateField()

    # calculated fields
    days_until_expiration = models.IntegerField()
    time_fraction_until_expiration = models.FloatField()
    # interest_rate = models.FloatField()
    # mid = models.FloatField()
    # implied_volatility_mid = models.FloatField(null=True)

    def __str__(self):
        return f'Quote data for {self.stock} stock'


class StockPrice(models.Model):

    open = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    close = models.FloatField()
    volume = models.FloatField()

    class Meta:
        abstract = True


class StockPriceHistory(StockPrice):

    stock = models.ForeignKey(Stock, on_delete=models.SET_NULL, null=True,
                              related_name='price_history')
    price = models.FloatField()
    datetime = models.DateTimeField()

    def __str__(self):
        return f'Price data for {self.stock} at {self.datetime}'


class DailyStockPriceHistory(StockPrice):

    stock = models.ForeignKey(Stock, on_delete=models.SET_NULL, null=True,
                              related_name='daily_price_history')
    date = models.DateField()

    def __str__(self):
        return f'Daily price data for {self.stock} at {self.date}'
