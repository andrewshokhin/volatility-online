import numpy as np
import pandas as pd
from scipy.stats import norm
from scipy.optimize import brentq


def N(z):
    """ Normal cumulative density function

    :param z: point at which cumulative density is calculated
    :return: cumulative density under normal curve
    """
    return norm.cdf(z)


def black_scholes_call_value(S, K, r, t, vol):
    """ Black-Scholes call option

    :param S: underlying
    :param K: strike price
    :param r: rate
    :param t: time to expiration
    :param vol: volatility
    :return: BS call option value
    """
    d1 = (1.0 / (vol * np.sqrt(t))) * (np.log(S / K) + (r + 0.5 * vol ** 2.0) * t)
    d2 = d1 - (vol * np.sqrt(t))

    return N(d1) * S - N(d2) * K * np.exp(-r * t)


def black_scholes_put_value(S, K, r, t, vol):
    """ Black-Scholes put option

    :param S: underlying
    :param K: strike price
    :param r: rate
    :param t: time to expiration
    :param vol: volatility
    :return: BS put option value
    """
    d1 = (1.0 / (vol * np.sqrt(t))) * (np.log(S / K) + (r + 0.5 * vol ** 2.0) * t)
    d2 = d1 - (vol * np.sqrt(t))

    return N(-d2) * K * np.exp(-r * t) - N(-d1) * S


def call_implied_volatility_objective_function(S, K, r, t, vol, call_option_market_price):
    """ Objective function which sets market and model prices to zero

    :param S: underlying
    :param K: strike price
    :param r: rate
    :param t: time to expiration
    :param vol: volatility
    :param call_option_market_price: market observed option price
    :return: error between market and model price
    """
    return call_option_market_price - black_scholes_call_value(S, K, r, t, vol)


def call_implied_volatility(S, K, r, t, call_option_market_price, a=-2.0, b=2.0, xtol=1e-6):
    """ Call implied volatility function

    :param S: underlying
    :param K: strike price
    :param r: rate
    :param t: time to expiration
    :param call_option_market_price: market observed option price
    :param a: lower bound for brentq method
    :param b: upper bound for brentq method
    :param xtol: tolerance which is considered good enough
    :return: volatility to sets the difference between market and model price to zero

    """
    # avoid mirroring outer scope
    _S, _K, _r, _t, _call_option_market_price = S, K, r, t, call_option_market_price

    # define a nested function that takes our target param as the input
    def fcn(vol):

        # returns the difference between market and model price at given volatility
        return call_implied_volatility_objective_function(_S, _K, _r, _t, vol, _call_option_market_price)

    # first we try to return the results from the brentq algorithm
    try:
        result = brentq(fcn, a=a, b=b, xtol=xtol)

        # if the results are *too* small, sent to np.nan so we can later interpolate
        return np.nan if result <= 1.0e-6 else result

    # if it fails then we return np.nan so we can later interpolate the results
    except ValueError:
        return np.nan


def put_implied_volatility_objective_function(S, K, r, t, vol, put_option_market_price):
    """ Objective function which sets market and model prices to zero

    :param S: underlying
    :param K: strike price
    :param r: rate
    :param t: time to expiration
    :param vol: volatility
    :param put_option_market_price: market observed option price
    :return: error between market and model price
    """
    return put_option_market_price - black_scholes_put_value(S, K, r, t, vol)


def put_implied_volatility(S, K, r, t, put_option_market_price, a=-2.0, b=2.0, xtol=1e-6):
    """ Put implied volatility function

    :param S: underlying
    :param K: strike price
    :param r: rate
    :param t: time to expiration
    :param put_option_market_price: market observed option price
    :param a: lower bound for brentq method
    :param b: upper gound for brentq method
    :param xtol: tolerance which is considered good enough
    :return: volatility to sets the difference between market and model price to zero

    """

    # avoid mirroring out scope
    _S, _K, _r, _t, _put_option_market_price = S, K, r, t, put_option_market_price

    # define a nsted function that takes our target param as the input
    def fcn(vol):

        # returns the difference between market and model price at given volatility
        return put_implied_volatility_objective_function(_S, _K, _r, _t, vol, _put_option_market_price)

    # first we try to return the results from the brentq algorithm
    try:
        result = brentq(fcn, a=a, b=b, xtol=xtol)

        # if the results are *too* small, sent to np.nan so we can later interpolate
        return np.nan if result <= 1.0e-6 else result

    # if it fails then we return np.nan so we can later interpolate the results
    except ValueError:
        return np.nan


def _get_implied_vol_mid(option_quote):
    """

    """
    option_type = option_quote['type']
    S = option_quote['underlying_price']
    K = option_quote['strike']
    r = option_quote['interest_rate']
    t = option_quote['time_fraction_until_expiration']
    mid = option_quote['mid']

    if option_type == 'call':
        method = call_implied_volatility
    elif option_type == 'put':
        method = put_implied_volatility
    else:
        raise ValueError(f'Unknown option type {option_type}')

    return float(method(S, K, r, t, mid))


def _interp_implied_volatility(options_frame):
    """ Interpolate missing (np.nan) values of implied volatility
    We first need to split the chains into expiration and type because we cannot
    interpolate across the entire chain, rather within these two groups

    :param options_frame: DataFrame containing options data
    :return original DataFrame with ImpliedVolatilityMid column containing interpolated values

    """
    # create a MultiIndex with Expiration, OptionType, the Strike as index, then sort
    frame = options_frame.reset_index().set_index(['expiration_date', 'type', 'strike']).sort_index()

    # remove duplicate indexes
    frame = frame.loc[~frame.index.duplicated(keep='first')]

    # pivot the vame with ImpliedVolatilityMid as the values within the table
    # this has Strikes along the rows and Expirations along the columns
    # the level=1 unstack pivots on Expiration and level=0 unstack pivots on OptionType
    unstacked = frame['implied_volatility'].unstack(level=1).unstack(level=0)

    # this line does three things:
    #    first interpolates across each Expiration date down the strikes for np.nan values
    #    second forward fills values which keeps the last interpolated value as the value to fill
    #    third back fills values which keeps the first interpolated value as the value to fill
    unstacked_interp = unstacked.interpolate().ffill().bfill()

    # restack into shape of original DataFrame
    unstacked_interp_indexed = unstacked_interp.stack(level=0).stack(level=0).reset_index()

    # replace old column with the new column with interpolated and filled values
    frame['implied_volatility'] = unstacked_interp_indexed.set_index(['expiration_date', 'type', 'strike'])

    # give our index back
    frame.reset_index(inplace=True)

    # return
    return frame


def calculate_implied_volatility(quotes: pd.DataFrame, prices: pd.DataFrame):
    prices = prices.reindex(quotes.index.unique(), method='nearest')
    quotes = pd.merge(quotes, prices, left_index=True, right_index=True)

    quotes['implied_volatility'] = quotes.apply(_get_implied_vol_mid, axis=1)
    quotes = _interp_implied_volatility(quotes)
    return quotes
