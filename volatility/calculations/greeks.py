"""
Taken from https://github.com/jasonstrimpel/PyData-Meetup
"""
import numpy as np
from scipy.stats import norm


def N(z):
    """ Normal cumulative density function

    :param z: point at which cumulative density is calculated
    :return: cumulative density under normal curve
    """
    return norm.cdf(z)


def black_scholes_call_value(S, K, r, t, vol):
    """ Black-Scholes call option

    :param S: underlying
    :param K: strike price
    :param r: rate
    :param t: time to expiration
    :param vol: volatility
    :return: BS call option value
    """
    d1 = (1.0 / (vol * np.sqrt(t))) * (np.log(S / K) + (r + 0.5 * vol ** 2.0) * t)
    d2 = d1 - (vol * np.sqrt(t))

    return N(d1) * S - N(d2) * K * np.exp(-r * t)


def black_scholes_put_value(S, K, r, t, vol):
    """ Black-Scholes put option

    :param S: underlying
    :param K: strike price
    :param r: rate
    :param t: time to expiration
    :param vol: volatility
    :return: BS put option value
    """
    d1 = (1.0 / (vol * np.sqrt(t))) * (np.log(S / K) + (r + 0.5 * vol ** 2.0) * t)
    d2 = d1 - (vol * np.sqrt(t))

    return N(-d2) * K * np.exp(-r * t) - N(-d1) * S

def phi(x):
    """ Phi helper function

    """
    return np.exp(-0.5 * x * x) / (np.sqrt(2.0 * np.pi))


def call_delta(S, K, r, t, vol):
    """ Black-Scholes call delta

    :param S: underlying
    :param K: strike price
    :param r: rate
    :param t: time to expiration
    :param vol: volatility
    :return: call delta
    """
    d1 = (1.0 / (vol * np.sqrt(t))) * (np.log(S / K) + (r + 0.5 * vol ** 2.0) * t)

    return N(d1)


def put_delta(S, K, r, t, vol):
    """ Black-Scholes put delta

    :param S: underlying
    :param K: strike price
    :param r: rate
    :param t: time to expiration
    :param vol: volatility
    :return: put delta
    """
    d1 = (1.0 / (vol * np.sqrt(t))) * (np.log(S / K) + (r + 0.5 * vol ** 2.0) * t)

    return N(d1) - 1.0


def _get_delta(quotes):
    """ Return the option delta given the OptionType

    :param quotes: row of the dataframe, accessible by label
    :return: option delta

    """
    option_type = quotes['type']
    S = quotes['underlying_price']
    K = quotes['strike']
    r = quotes['interest_rate']
    t = quotes['time_fraction_until_expiration']
    vol = quotes['implied_volatility']

    if option_type == 'call':
        method = call_delta
    elif option_type == 'put':
        method = put_delta
    else:
        raise ValueError(f'Unknown option type {option_type}')
    return method(S, K, r, t, vol)


def calculate_delta(quotes):
    quotes['delta'] = quotes.apply(_get_delta, axis=1)
    return quotes
