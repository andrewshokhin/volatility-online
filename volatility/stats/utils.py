import pandas as pd

from market_data.models import Stock


def get_daily_price_data(stock: Stock) -> pd.DataFrame:
    df = pd.DataFrame.from_records(
        stock.daily_price_history
        .values('date', 'open', 'high', 'low', 'close')
        .order_by('date'),

    )
    if not df.empty:
        df = df.set_index('date')
    return df
