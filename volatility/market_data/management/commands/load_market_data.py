import logging
from typing import List

from requests.exceptions import BaseHTTPError
from django.core.management import BaseCommand
from django.conf import settings
from django.db import transaction, DatabaseError
from django.db.models import Max

from market_data.models import OptionQuote, Stock, StockPriceHistory, DailyStockPriceHistory
from market_data.tradier_client import TradierClient

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    help = ('Requests fresh market data (option quotes and stock prices)\n'
            'from tradier.com API and saves it into the database')

    def add_arguments(self, parser):
        parser.add_argument('-b', '--base-api-url', type=str, default=settings.TRADIER_API_ROOT)
        parser.add_argument('-t', '--access-token', type=str, default=settings.TRADIER_API_ACCESS_TOKEN)

    def handle(self, *args, **options):
        """Iterate over all stocks and update data for them."""
        logger.info(f'Starting data loading from {options["base_api_url"]}')
        client = TradierClient(
            base_api_url=options['base_api_url'],
            access_token=options['access_token']
        )
        for stock in Stock.objects.filter(active=True).all():
            try:
                self.update_stock_data(client, stock)
            except (BaseHTTPError, DatabaseError, ValueError,
                    AttributeError, TypeError):
                logger.exception(f'Got an error processing {stock.symbol}, skipping',
                                 exc_info=True)
        logger.info(f'Data update finished successfully')

    def update_stock_data(self, client: TradierClient,  stock: Stock):
        logger.info(f'Downloading data for "{stock.symbol}" stock')
        quotes = []
        logger.info('Requesting option chains')
        for expiration_date in client.request_expiration_dates(stock):
            quotes.extend(client.request_quotes(stock, expiration_date))

        logger.info('Requesting online prices history')
        stock_price_history = client.request_price_history(stock)

        logger.info('Requesting daily prices history')
        daily_prices = client.request_daily_price_data(stock)

        logger.info('Saving data into the database')
        with transaction.atomic():
            # create only new quotes
            new_quotes = self.filter_new_quotes(quotes)
            OptionQuote.objects.bulk_create(new_quotes)

            # rewrite history from the first timestamp in response
            first_dt = min(p.datetime for p in stock_price_history)
            stock.price_history.filter(datetime__gte=first_dt).delete()
            StockPriceHistory.objects.bulk_create(stock_price_history)

            new_daily_prices = self.filter_new_daily_prices(stock, daily_prices)
            DailyStockPriceHistory.objects.bulk_create(new_daily_prices)

        logger.info(f'Processing "{stock.symbol}" stock is done')

    @staticmethod
    def filter_new_quotes(quotes: List[OptionQuote]) -> List[OptionQuote]:
        """Check which quotes are already in database and filter them out."""
        existing_quotes = set(OptionQuote.objects
                              .filter(identifier__in=(q.identifier for q in quotes))
                              .values_list('identifier', flat=True))
        return [q for q in quotes if q.identifier not in existing_quotes]

    @staticmethod
    def filter_new_daily_prices(stock: Stock, daily_prices: List[DailyStockPriceHistory]
                                ) -> List[DailyStockPriceHistory]:
        last_date = stock.daily_price_history.aggregate(last=Max('date')).get('last')
        if not last_date:
            # first run for this stock, save all the data we received
            return daily_prices
        else:
            # it also may empty list
            return [p for p in daily_prices if p.date > last_date]
