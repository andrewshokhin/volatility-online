from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),

    path('stock-autocomplete/', views.StockAutocomplete.as_view(), name='stock_autocomplete'),
]
