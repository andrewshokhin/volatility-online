from datetime import date

import pandas as pd
from bokeh.embed import components
from bokeh.models import ColumnDataSource, HoverTool
from bokeh.plotting import figure
from django.db.models import Max, Min, F, Case, When, Window, Value, Count
from django.db.models.functions import RowNumber
from django.http import HttpResponseBadRequest
from django.shortcuts import render_to_response
from django.views import View

from market_data.models import Stock, OptionQuote
from stats.models import RealizedVolatility
from plots.forms import UniverseExplorerForm
from plots.views.utils import get_expiration_dates


class UniverseExplorerView(View):

    moneyness_options = [('atm_05', 'ATM (0.5d)'), ('otm_025', 'OTM (0.25d)'),
                         ('otm_01', 'OTM (0.1d)')]

    def get(self, request):
        form = UniverseExplorerForm(request.GET)
        if form.is_valid():
            options = self.get_options()
            parameters = self.set_defaults(form, options)
            context = self.get_context_data(parameters, options)
            return render_to_response('explorer.html', context)
        else:
            return HttpResponseBadRequest(form.errors.as_json())

    def get_options(self):
        options = {}
        agg = OptionQuote.objects.aggregate(max=Max('open_interest'), min=Min('open_interest'))
        options['open_interest_range'] = dict(agg)
        agg = OptionQuote.objects.aggregate(max=Max('volume'), min=Min('volume'))
        options['volume_range'] = dict(agg)
        options['moneyness_options'] = self.moneyness_options
        options['var_options'] = [(var.name, var.description) for var in AVAILABLE_VARIABLES]
        options['expiration_dates'] = get_expiration_dates(future_only=True)
        return options

    def set_defaults(self, form, options):
        parameters = dict(form.cleaned_data)
        if not parameters['min_open_interest']:
            parameters['min_open_interest'] = options['open_interest_range']['min']
        if not parameters['min_volume']:
            parameters['min_volume'] = options['volume_range']['min']
        if not parameters['min_expiration_date']:
            parameters['min_expiration_date'] = date.today()
        if not parameters['moneyness']:
            parameters['moneyness'] = options['moneyness_options'][0][0]
        if not parameters['x_var']:
            parameters['x_var'] = AVAILABLE_VARIABLES[0].name
        if not parameters['y_var']:
            parameters['y_var'] = AVAILABLE_VARIABLES[1].name
        return parameters

    def get_context_data(self, parameters, options):
        plot = self.get_plot(**parameters)
        script, div = components(plot)
        context = {'plot_script': script, 'plot_div': div,
                   'selected_open_interest': parameters['min_open_interest'],
                   'selected_volume': parameters['min_volume'],
                   'selected_expiration_date': parameters['min_expiration_date'],
                   'selected_moneyness': parameters['moneyness'],
                   'selected_x_var': parameters['x_var'],
                   'selected_y_var': parameters['y_var']}
        context.update(options)
        return context

    @staticmethod
    def get_plot(x_var, y_var, **parameters):
        x_variable = ExplorerVariable.get_by_name(x_var)
        y_variable = ExplorerVariable.get_by_name(y_var)
        if x_variable.name == y_variable.name:
            data = x_variable.get_stocks_data(**parameters).to_frame()
        else:
            data = pd.concat([x_variable.get_stocks_data(**parameters),
                              y_variable.get_stocks_data(**parameters)],
                             axis=1, sort=True)
        data.index.name = 'symbol'

        source = ColumnDataSource(data)
        plot = figure(title=f'Universe explorer',
                      x_axis_label=x_variable.description,
                      y_axis_label=y_variable.description)
        # Add big white circles for better hover tool experience
        plot.circle(x=x_variable.name, y=y_variable.name, color='white', size=30, source=source)
        plot.circle(x=x_variable.name, y=y_variable.name, size=5, source=source)
        plot.add_tools(HoverTool(tooltips=[
            (x_variable.description, f'@{x_variable.name}'),
            (y_variable.description, f'@{y_variable.name}'),
            ('Stock', f'@{data.index.name}')]))
        return plot


class ExplorerVariable:
    """
    Base class for variable in universe explorer.

    User can select two variables there (one foe x axis and one for y).
    This class represents a single option.
    """

    # internal identifier
    name = 'variable'
    # representation for user
    description = 'Variable Description'

    @classmethod
    def get_stocks_data(cls, min_open_interest: float, min_volume: float,
                        min_expiration_date: date, moneyness: float) -> pd.Series:
        """
        Return variable values for each stock.

        arguments are user specified parameters in interface.
        return value should be a pandas.Series with stock symbol as index.
        """
        raise NotImplementedError

    @classmethod
    def get_by_name(cls, name):
        for klass in cls.__subclasses__():
            if klass.name == name:
                return klass
            # check all subclasses recursively
            subclass = klass.get_by_name(name)
            if subclass is not None:
                return subclass
        else:
            return None


class BaseImpliedVolatilityVariable(ExplorerVariable):
    """Base class for put and call implied volatility variables."""

    option_type = 'type'

    @staticmethod
    def get_delta(moneyness):
        raise NotImplementedError

    @classmethod
    def get_stocks_data(cls, min_open_interest: float, min_volume: float,
                        min_expiration_date: date, moneyness: str) -> pd.Series:
        stocks_count = Stock.objects.count()
        delta = cls.get_delta(moneyness)
        qs = (OptionQuote.objects
              .filter(volume__gte=min_volume,
                      open_interest__gte=min_open_interest,
                      type=cls.option_type,
                      expiration_date__gte=min_expiration_date)
              .annotate(distance=Case(
                # distance = ABS(delta_greek - delta)
                When(stats__delta_greek__gte=delta, then=F('stats__delta_greek') - delta),
                When(stats__delta_greek__lt=delta, then=delta - F('stats__delta_greek')),
              ))
              # we need to find a quote with the nearest delta
              # so we calculate row_number(OVER distance PARTITION BY stock_id)
              # and then select records with row_number = 1
              .annotate(row_number=Window(
                expression=RowNumber(),

                partition_by=[F('stock_id')],
                order_by=F('distance').asc(),
              ))
              # it's a bit hackish. django doesn't allow to filter by window functions
              # but we know how many records there should be (the same as number of stock)
              # so we order by row_number
              .order_by('row_number')[:stocks_count]
              # and take N first records
              .values('stock__symbol', 'stats__implied_volatility', 'row_number'))
        df = (pd.DataFrame.from_records(qs))
        # some stocks can have no records for specified filters, filter them in python
        df = df[df['row_number'] == 1].drop('row_number', axis=1)
        df = (df.rename(columns={'stock__symbol': 'symbol', 'stats__implied_volatility': cls.name})
              .set_index('symbol'))[cls.name]
        return df


class PutImpliedVolatilityVariable(BaseImpliedVolatilityVariable):

    name = 'put_iv'
    description = 'Put I.V.'

    option_type = OptionQuote.PUT

    @staticmethod
    def get_delta(moneyness):
        # put options have negative deltas, for them we need to inverse sign
        if moneyness == 'atm_05':
            return -0.5
        elif moneyness == 'otm_025':
            return -0.25
        elif moneyness == 'otm_01':
            return -0.1


class CallImpliedVolatilityVariable(BaseImpliedVolatilityVariable):

    name = 'call_iv'
    description = 'Call I.V.'

    option_type = OptionQuote.CALL

    @staticmethod
    def get_delta(moneyness):
        # return moneyness as is
        if moneyness == 'atm_05':
            return 0.5
        elif moneyness == 'otm_025':
            return 0.25
        elif moneyness == 'otm_01':
            return 0.1


class PutCallImpliedVolatilityVariable(ExplorerVariable):

    name = 'put_call_iv'
    description = 'Put+Call I.V.'

    @classmethod
    def get_stocks_data(cls, **parameters) -> pd.Series:
        put = PutImpliedVolatilityVariable.get_stocks_data(**parameters)
        call = CallImpliedVolatilityVariable.get_stocks_data(**parameters)
        result = (put + call) / 2
        result.name = cls.name
        return result


class BaseIVPercentileVariable(ExplorerVariable):

    underlying_variable = BaseImpliedVolatilityVariable
    option_types = ['type']

    @classmethod
    def get_stocks_data(cls, **parameters) -> pd.Series:
        current_values = cls.underlying_variable.get_stocks_data(**parameters)
        # find number of records where given value is greater then historical
        qs = (OptionQuote.objects
              .filter(type__in=cls.option_types)
              .annotate(diff=Case(
                *[When(stock__symbol=symbol,
                       then=Value(current_value) - F('stats__implied_volatility'))
                  for symbol, current_value in current_values.iteritems()]
               ))
              .filter(diff__gte=0)
              .values('stock__symbol')
              .annotate(partial=Count('id')))
        partial = (pd.DataFrame.from_records(qs)
                   .rename(columns={'stock__symbol': 'symbol'})
                   .set_index('symbol')['partial'])
        # and total number of records in history
        qs = (OptionQuote.objects
              .values('stock__symbol')
              .annotate(total=Count('id')))
        total = (pd.DataFrame.from_records(qs)
                 .rename(columns={'stock__symbol': 'symbol'})
                 .set_index('symbol')['total'])
        # and now simply divide them to get percentile
        percentiles = partial / total * 100
        percentiles.name = cls.name
        return percentiles


class PutIVPercentileVariable(BaseIVPercentileVariable):

    name = 'put_iv_percentile'
    description = 'Put I.V. Perc'

    underlying_variable = PutImpliedVolatilityVariable
    option_types = [OptionQuote.PUT]


class CallIVPercentileVariable(BaseIVPercentileVariable):

    name = 'call_iv_percentile'
    description = 'Call I.V. Perc'

    underlying_variable = CallImpliedVolatilityVariable
    option_types = [OptionQuote.CALL]


class PutCallIVPercentileVariable(BaseIVPercentileVariable):

    name = 'put_call_iv_percentile'
    description = 'Put+Call I.V. Perc'

    underlying_variable = PutCallImpliedVolatilityVariable
    option_types = [OptionQuote.PUT, OptionQuote.CALL]


class RealizedVolatilityVariable(ExplorerVariable):

    name = 'rv'
    description = 'R.V.'

    @classmethod
    def get_stocks_data(cls, **parameters) -> pd.Series:
        stocks_count = Stock.objects.count()
        # get the latest values for each stock
        qs = (RealizedVolatility.objects
              .annotate(row_number=Window(
                  expression=RowNumber(),

                  partition_by=[F('stock_id')],
                  order_by=F('date').desc(),
              ))
              .order_by('row_number')[:stocks_count]
              .values('stock__symbol', 'value', 'row_number'))
        df = (pd.DataFrame.from_records(qs))
        df = df[df['row_number'] == 1].drop('row_number', axis=1)
        df = (df.rename(columns={'stock__symbol': 'symbol', 'value': cls.name})
              .set_index('symbol'))[cls.name]
        return df


class RVPercentileVariable(ExplorerVariable):

    name = 'rv_percentile'
    description = 'R.V. Perc'

    underlying_variable = RealizedVolatilityVariable

    @classmethod
    def get_stocks_data(cls, **parameters) -> pd.Series:
        current_values = cls.underlying_variable.get_stocks_data(**parameters)
        # find number of records where given value is greater then historical
        qs = (RealizedVolatility.objects
              .annotate(diff=Case(
                *[When(stock__symbol=symbol,
                       then=Value(current_value) - F('value'))
                  for symbol, current_value in current_values.iteritems()]
               ))
              .filter(diff__gte=0)
              .values('stock__symbol')
              .annotate(partial=Count('id')))
        partial = (pd.DataFrame.from_records(qs)
                   .rename(columns={'stock__symbol': 'symbol'})
                   .set_index('symbol')['partial'])
        # and total number of records in history
        qs = (RealizedVolatility.objects
              .values('stock__symbol')
              .annotate(total=Count('id')))
        total = (pd.DataFrame.from_records(qs)
                 .rename(columns={'stock__symbol': 'symbol'})
                 .set_index('symbol')['total'])
        # and now simply divide them to get percentile
        percentiles = partial / total * 100
        percentiles.name = cls.name
        return percentiles


AVAILABLE_VARIABLES = [
    PutImpliedVolatilityVariable, CallImpliedVolatilityVariable,
    PutCallImpliedVolatilityVariable,
    PutIVPercentileVariable, CallIVPercentileVariable,
    PutCallIVPercentileVariable,

    RealizedVolatilityVariable,
    RVPercentileVariable,
]
