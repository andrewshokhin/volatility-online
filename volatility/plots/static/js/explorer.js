// parse variables from django
function load_universe_explorer() {
    var openInterestRange = JSON.parse($("#open-interest-range").text());
    var volumeRange = JSON.parse($("#volume-range").text());
    var expirationDates = JSON.parse($('#expiration-dates').text());
    var parameters = {
        openInterest: JSON.parse($("#selected-open-interest").text()),
        volume: JSON.parse($("#selected-volume").text()),
        expirationDate: JSON.parse($("#selected-expiration-date").text()),
        moneyness: JSON.parse($("#selected-moneyness").text()),
        xVar: JSON.parse($("#selected-x-var").text()),
        yVar: JSON.parse($("#selected-y-var").text())
    };

    function reload_plot() {
        // form request to django server
        var explorer_url = "/plots/universe_explorer?" + $.param({
            min_open_interest: parameters.openInterest,
            min_volume: parameters.volume,
            min_expiration_date: parameters.expirationDate,
            moneyness: parameters.moneyness,
            x_var: parameters.xVar,
            y_var: parameters.yVar
        });
        $.ajax({
            type: 'GET',
            url: explorer_url,
            success: function (content) {
                $("#universe_explorer_plot").replaceWith(content);
            }
        });

    }

    // Sliders are in logarithmic scale starting from value 2
    function linearToLog(n){
        if (n <= 1){
            return n
        }
        return Math.log(n);
    }
    function logToLinear(n){
        if (n <= 1){
            return n
        }
        return Math.round(Math.exp(n));

    }
    $("#open-interest-slider").ionRangeSlider({
        type: "double",
        grid: true,
        from: linearToLog(parameters.openInterest),
        to: linearToLog(openInterestRange.max),
        step: 0.1,
        to_fixed: true,
        min: linearToLog(openInterestRange.min),
        max: linearToLog(openInterestRange.max),
        prettify_enabled: true,
        prettify: logToLinear,
        onFinish: function (data) {
            parameters.openInterest = logToLinear(data.from);
            reload_plot();
        },
    });

    $("#volume-slider").ionRangeSlider({
        type: "double",
        grid: true,
        from: linearToLog(parameters.volume),
        to: linearToLog(volumeRange.max),
        step: 0.1,
        to_fixed: true,
        min: linearToLog(volumeRange.min),
        max: linearToLog(volumeRange.max),
        prettify_enabled: true,
        prettify: logToLinear,
        onFinish: function (data) {
            parameters.volume = logToLinear(data.from);
            reload_plot();
        },
    });

    $('#datepicker .input-group.date').datepicker({
        format: 'yyyy-m-d',
        autoclose: true,
        multidate: false,
        beforeShowDay: function(date) {
            // var idx = $.inArray(formatDate(date), expirationDates)
            // console.log(formatDate(date), idx, date);
            return $.inArray(date.toISOString().slice(0, 10), expirationDates) != -1;
        }
    }).on('changeDate', function(ev) {
        var selected_date = $('#datepicker .input-group.date').datepicker('getDate');
        parameters.expirationDate = selected_date.toISOString().slice(0, 10);
        reload_plot()
        // let $implied_volatility_url = "{% url 'plots:skew_chart' stock_symbol=selected_stock.symbol %}?expiration_dates=" + formatted;
        // $("#implied_volatility_plot").load($implied_volatility_url);

    });

    $('#moneyness-selector').select2({
        'width': 'style'
    }).on('select2:select', function (e) {
        parameters.moneyness = e.params.data.id;
        reload_plot();
    });
    $('#x-var-selector').select2({
        'width': 'style'
    }).on('select2:select', function (e) {
        parameters.xVar = e.params.data.id;
        reload_plot();
    });
    $('#y-var-selector').select2({
        'width': 'style'
    }).on('select2:select', function (e) {
        parameters.yVar = e.params.data.id;
        reload_plot();
    });
}