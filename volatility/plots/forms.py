from django import forms


class MultipleDateField(forms.DateField):

    def to_python(self, value):
        if value in self.empty_values:
            return []
        return [super(forms.DateField, self).to_python(v) for v in value.split(',')]


class ExpirationDatesListForm(forms.Form):
    expiration_dates = MultipleDateField(required=False)


class StrikeForm(forms.Form):
    strike = forms.FloatField(required=False)


class WindowForm(forms.Form):
    window = forms.IntegerField(required=False)


class UniverseExplorerForm(forms.Form):
    min_open_interest = forms.FloatField(required=False)
    min_volume = forms.FloatField(required=False)
    min_expiration_date = forms.DateField(required=False)
    moneyness = forms.CharField(required=False)
    x_var = forms.CharField(required=False)
    y_var = forms.CharField(required=False)
